# -*- coding: utf-8 -*-

import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import smtplib
import imaplib
import time
import yaml
import csv
import xml.etree.ElementTree as ET
import argparse
import os
import glob
import re
import sh
from sh import qpdf
import logging
import time
import schedule
from collections import deque
import datetime
import sys


tries = 4  # número de intentos
t = 10     # tiempo entre intentos de envío

class DocMailSender():
    """
    Document emails sender class
    note to self:
        the element with tag E34 contains the service-nr
        the element with tag E3 contains the number of the document
        the element with tag E2 contains the document type
        in Glosas the element with the tag GMAIL contains the email address to use as the recipient
    """

    def __init__(self, config_file, XML_file, CSV_file):
        with open(config_file, 'r') as yaml_file:
            self.cfg = yaml.safe_load(yaml_file)

        # Initialize IMAP and SMTP server objects

        self.IMAP_Host = self.cfg['server']['imap_host']
        self.IMAP_Port = self.cfg['server']['imap_port']
        self.SMTP_Host = self.cfg['server']['smtp_host']

        self.sender         = self.cfg['mail_account']['sender']
        self.sender_alias   = self.cfg['mail_account']['sender_alias']
        self.subject_prefix = self.cfg['mail_details']['subject_prefix']
        self.subject_suffix = self.cfg['mail_details']['subject_suffix']
        self.passwd         = self.cfg['mail_account']['passwd']
           
        # Initialize XML parser
        self.tree = ET.parse(XML_file)

        # Initialize CSV parser
        self.CSV_file = CSV_file

        self.q = deque()
        self.period     = self.cfg['scheduling']['period']
        self.unit       = self.cfg['scheduling']['unit']
        self.bunch_size = self.cfg['scheduling']['bunch_size']
        if self.unit not in ['minutes', 'hours']:
            #logging.error('Unknown period unit for scheduling in configuration')
            logging.error('Unidad de tiempo desconocida en configuración del perído de programación de envíos')
            #sys.exit('Unknown period unit for scheduling in configuration')
            sys.exit('Unidad de tiempo desconocida en configuración del perído de programación de envíos')

        # Reporting
        self.result = {'errors': {'count': 0}, 'sent': {'count': 0}}
        self.report_recipients = self.cfg['report']['recipients']

    def send_document_mail(self, account_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, attachment):
        '''
        This function sends the email through SMTP and stores it in the Sent folder
        through IMAP. The emails to send contain information about the service
        bills.
        It receives the service or account number and a list of the email addresses
        that should receive the information.
        '''
        msg = self.write_msg(account_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, attachment)

        for i in range(tries):
            try:
                self.send_2_server(msg, account_nr, doc_type, attachment)
            #except KeyError as e:
            except:
                if i < tries - 1: # i is zero indexed
                    logging.info('Error al enviar correo. Reintentando en ' + str(t) + ' segundos.')
                    time.sleep(t)
                    continue
                else:
                    logging.info('Envío de correo falló ' + str(tries) + ' veces.')
                    raise
            break

    def send_2_server(self, msg, account_nr, doc_type, attachment):

        self.smtp_server = smtplib.SMTP(self.SMTP_Host)
        self.smtp_server.login(self.sender, self.passwd)

        self.imap_server = imaplib.IMAP4_SSL(self.IMAP_Host)
        self.imap_server.login(self.sender, self.passwd)
        
        self.smtp_server.send_message(msg)
        self.imap_server.append('INBOX.Sent', r'(\SEEN)', imaplib.Time2Internaldate(time.time()), str(msg).encode('utf-8'))
        #logging.info(os.path.basename(attachment)+' sent to addresses ('+msg['To']+') of client(s) of service nr '+account_nr)
        logging.info(os.path.basename(attachment)+' enviada a correos ('+msg['To']+') de cliente(s) del servicio '+account_nr)
        self.count_sent(doc_type)

        self.smtp_server.quit()
        self.imap_server.logout()

    def write_msg(self, account_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, attachment):

        doctype_str = self.cfg['document_types'][doc_type]

        msg = MIMEMultipart()
        #msg['From'] = "{} <{}>".format(self.sender_alias, self.sender)
        # The line above would be ideal, yet because of misconfigurations in the
        # server plus bugs only the line below works. If it ain't broke don't fix it
        msg['From'] = self.sender
        COMMASPACE = ', '
        msg['To'] = COMMASPACE.join(recipients)
        msg['Subject'] = self.subject_prefix + account_nr + self.subject_suffix
        msg.preamble = 'Cuenta del servicio de agua potable'
        text = self.cfg['mail_details']['body_base'].format(client_name, doctype_str, doc_id, due_date)\
                + '\n' + client_msgs \
                + '\n\n' + self.cfg['mail_details']['body_end']
        m_text = MIMEText(text, 'plain')
        msg.attach(m_text)

        with open(attachment, 'rb') as fp:
            pdf_doc = MIMEApplication(fp.read())
            pdf_doc.add_header('Content-Disposition', 'attachment', filename=os.path.basename(attachment))
        msg.attach(pdf_doc)

        return msg
        

    def parse_xml_info(self, services_dict):
        root = self.tree.getroot()
        out_dicts_dict = dict()
        service_nr_xpath = self.cfg['XML_parser']['service_nr_xpath']
        client_name_xpath= self.cfg['XML_parser']['client_name_xpath']
        doc_nr_xpath     = self.cfg['XML_parser']['doc_nr_xpath']
        due_date_xpath   = self.cfg['XML_parser']['due_date_xpath']
        doc_type_xpath   = self.cfg['XML_parser']['doc_type_xpath']
        mail_addr_xpath  = self.cfg['XML_parser']['mail_addr_xpath']
        if args['messages']:
            msgs_xpath   = self.cfg['XML_parser']['msgs_xpath']
            msg_snips    = self.cfg['XML_parser']['msg_snippets']

        for service_nr in services_dict:

            document     = root.find(service_nr_xpath.format(service_nr))
            
            if document == None:
                logging.error('Service {} not found in XML.'.format(service_nr))
                self.count_error(service_nr, 'service_not_in_XML')
            else:
                client_name  = document.find(client_name_xpath).text
                doc_id       = document.find(doc_nr_xpath).text
                due_date_obj = datetime.datetime.strptime(document.find(due_date_xpath).text,'%Y%m%d')
                due_date     = due_date_obj.strftime('%d/%m/%Y')
                doc_type     = document.find(doc_type_xpath).text
                address      = document.find(mail_addr_xpath)
                addresses    = []
                if address != None:
                    address   = address.text.lower()
                    if address not in services_dict[service_nr]['addresses']:
                        addresses.append(address)
                addresses.extend(services_dict[service_nr]['addresses'])
                message_text = ''
                if args['messages']:
                    message_els = document.findall(msgs_xpath)
                    messages = []
                    for msg_el in message_els:
                        this_message = msg_el.text
                        for snippet in msg_snips:
                            if snippet in this_message:
                                messages.append(this_message.strip('#'))
                    message_text = '\n- '.join(messages)
                    if len(messages)>0:
                        message_text = 'Notificaciones:\n- '+message_text 

                service_out_dict = {'client_name':client_name, 'doc_id':doc_id, 'due_date':due_date, 'doc_type':doc_type, 'addresses':addresses, 'messages':message_text}
                out_dicts_dict[service_nr] = service_out_dict

        return out_dicts_dict

    def parse_csv_info(self):
        '''
        This function parses the info provided in the csv file. It should contain a
        column with the service numbers and further columns with one email address each
        '''
        services_dict = dict() 

        with open(self.CSV_file) as csvfile:
            csv_reader = csv.DictReader(csvfile)

            for row in csv_reader:
                addresses = set()
                service = row['SERVICE'].upper()
                for col in row:
                    addresses.add(row[col].lower())
                if service.lower() in addresses:
                    addresses.remove(service.lower())
                addresses = list(addresses)
                if service in services_dict:
                    services_dict[service]['addresses'].extend(addresses)
                else:
                    services_dict[service] = dict()
                    services_dict[service]['addresses'] = addresses

        return services_dict

    def set_doc_apart(self, type_ranges, doc_type, doc_id):
        '''
        This function takes the page corresponding to the doc_id in the appropriate file (navigating
        the map of files provided in a dictionary called type_ranges) and writes it using qpdf to a
        file in the doc_filename path, which is returned for further processing
        '''
        pdfs_dir = 'pdf_docs'
        if not os.path.exists(pdfs_dir):
            os.makedirs(pdfs_dir)
        doc_filename = ''
        doc_type = int(doc_type)
        doctype_str  = self.cfg['document_types_compat'][doc_type]
        for doc_nums in type_ranges[doc_type]:
            first_doc = type_ranges[doc_type][doc_nums][0]
            last_doc  = type_ranges[doc_type][doc_nums][1]
            if doc_id >= first_doc and doc_id <= last_doc and type_ranges[doc_type][doc_nums][3]:
                doc_filename = pdfs_dir+'/'+doctype_str+'_'+str(doc_id)+'.pdf'
                doc_page = doc_id - first_doc + 1
                qpdf('--empty', '--pages', doc_nums, doc_page, '--', doc_filename)
                break
        return doc_filename

    def sched_document_mail(self, account_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, attachment):
        '''
        This function schedules the sending of the document for its later execution at regular
        intervals for a bunch of documents.
        This should take into account the limitations on sending email in the particular mailing
        system of the used hosting.
        '''
        self.q.append({'account_nr':account_nr, 'client_name':client_name, 'doc_id':doc_id, 'due_date':due_date, 'client_msgs':client_msgs, 'recipients':recipients, 'doc_type':doc_type, 'attachment':attachment})

    def send_bunch(self):

        bunch_size = self.cfg['scheduling']['bunch_size']
        remaining = len(self.q)
        logging.info('{} emails remaining.'.format(remaining))
        due_mails = min(bunch_size, remaining)
        logging.info('Sending {} emails in this bunch.'.format(due_mails))

        for i in range(due_mails):
            send_info = self.q.popleft()
            logging.info('Enviando '+os.path.basename(send_info['attachment'])+' a cliente(s) del servicio '+send_info['account_nr'])
            self.send_document_mail(**send_info)
            os.remove(send_info['attachment'])
        
        if len(self.q)==0:
            schedule.clear()
       #else:
       #    logging.info('Sending more docs again at '+schedule.next_run().strftime('%H:%M:%S'))
            
    def count_sent(self, doctype):
        self.result['sent']['count'] += 1
        if doctype not in self.result['sent']:
            self.result['sent'][doctype] = 0
        self.result['sent'][doctype] += 1

    def count_error(self, service_nr, error_name):
        self.result['errors']['count'] += 1
        if error_name not in self.result['errors']:
            self.result['errors'][error_name] = []
        self.result['errors'][error_name].append(service_nr)

    def write_report(self):
        text = ''
        if self.result['sent']['count'] == 1:
            text += 'Se envió 1 documento.\n'
        else:
            text += 'Se envió {} documentos.\n'.format(self.result['sent']['count'])
        for doctype in self.result['sent']:
            if doctype == 'count':
                continue
            text += ' '+self.cfg['document_types'][doctype]+': {}\n'.format(self.result['sent'][doctype])
        if self.result['errors']['count'] == 0:
            text += '\nNo hubo errores.\n'
        elif self.result['errors']['count'] == 1:
            text += '\nHubo 1 error.\n'
        else:
            text += '\nHubo {} errores.\n'.format(self.result['errors']['count'])
        if self.result['errors']['count'] > 0:
            text += 'Los errores son los siguientes:\n\n '
            for error in self.result['errors']:
                if error == 'count':
                    continue
                text += error+':\n  '
                for service in self.result['errors'][error]:
                    text += service+'\n  '
                text += '\n\n '
        return text

    def send_report_mail(self):
        '''
        This function sends a report email with the results of running the script.
        It does so through SMTP and stores the message in the Sent folder
        through IMAP.
        The list of the email addresses that should receive the information is
        read from the config file.
        '''
        recipients = self.report_recipients

        self.smtp_server = smtplib.SMTP(self.SMTP_Host)
        self.smtp_server.login(self.sender, self.passwd)

        self.imap_server = imaplib.IMAP4_SSL(self.IMAP_Host)
        self.imap_server.login(self.sender, self.passwd)

        msg = MIMEMultipart()
        msg['From'] = self.sender
        COMMASPACE = ', '
        msg['To'] = COMMASPACE.join(recipients)
        msg['Subject'] = 'Reporte de envío de documentos de cobro'
        msg.preamble = 'Envio de cuentas de agua potable'
        text = self.write_report()
        m_text = MIMEText(text, 'plain')
        msg.attach(m_text)

        self.smtp_server.send_message(msg)
        self.imap_server.append('INBOX.Sent', '', imaplib.Time2Internaldate(time.time()), str(msg).encode('utf-8'))
        #logging.info('Report sent to the following addresses: '+msg['To'])
        logging.info('Reporte enviado a los siguientes correos: '+msg['To'])

        self.smtp_server.quit()
        self.imap_server.logout()


def organize_filenames(filenames_arg):
    '''
    returns a dictionary in this fashion:

    {39:{"file1":[345, 354, 10, True], "file2":[355, 364, 10, True]},
     33:{"file3":[445, 454, 9, False], "file4":[455, 464, 10, True]}}

    where the first set of keys (numbers) refers to document type codes,
    then follow the dictionaries indexed by filenames with values of a
    list of numbers which represent the first and last document ids in
    each particular file, the number of pages in the file and a boolean
    which reports if the file has all the pages its name states it has

    it has been observed that the software generating those PDFs someti-
    mes generates faulty files, which could result in sending not the
    propper document to some clients

    the files are named in the following fashion
    T39F43863-43911.pdf
    '''

    pdf_filenames = glob.glob(args['inputPDF'])
    pdf_ranges    = {filename : [int(num) for num in re.split('\D+',os.path.basename(filename)) if num != ''] for filename in pdf_filenames}
    doc_types     = {pdf_ranges[filename][0] for filename in pdf_ranges}
    type_ranges   = {int(doctype) : {filename : pdf_ranges[filename][-2:] for filename in pdf_ranges if pdf_ranges[filename][-3]==doctype} for doctype in doc_types}
    for doctype in type_ranges:
        for filename in type_ranges[doctype]:
            pages = int(qpdf('--show-npages', filename))
            type_ranges[doctype][filename].append(pages)
            type_ranges[doctype][filename].append(validate_pnum(type_ranges[doctype][filename]))
    return type_ranges

def validate_pnum(docsNpages):
    '''
    validates that the PDFs all have as many pages as they should
    have, according to their filenames
    '''
    docs_num = docsNpages[1] - docsNpages[0] + 1
    pages    = docsNpages[2]
    if docs_num == pages :
        return True
    else:
        return False

def main(args):

    logging.basicConfig(filename=args['logfile'], format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
    scheduling = args['schedule']
    dry_run = args['dryrun']

    docs_sender = DocMailSender(args['config'], args['xmlfile'], args['csvfile'])

    csv_info = docs_sender.parse_csv_info()
    xml_info = docs_sender.parse_xml_info(csv_info)

    type_ranges = organize_filenames(args['inputPDF'])

    faulty_files = []
    for doctype in type_ranges:
        faulty_files.extend([filename for filename in type_ranges[doctype] if not type_ranges[doctype][filename][3]])
    if len(faulty_files)>0:
        for filename in faulty_files:
            expected_pages = type_ranges[doctype][filename][1] - type_ranges[doctype][filename][0] + 1
            logging.error('El archivo '+filename+' debiera tener '+str(expected_pages)+' páginas, pero tiene '+str(type_ranges[doctype][filename][2]))
            #logging.error('The file '+filename+" should contain '+str(expected_pages)+' pages, yet it has "+str(type_ranges[doctype][filename][2]))
        sys.exit('Los archivos PDF provistos no tienen la cantidad de documentos que se espera. Ver logs para mayor detalle')
        #sys.exit("The provided PDFs don't contain the expected document count. Read logs for more details")


    for service_nr in xml_info:
        client_name  = xml_info[service_nr]['client_name']
        due_date     = xml_info[service_nr]['due_date']
        doc_id       = int(xml_info[service_nr]['doc_id'])
        doc_type     = int(xml_info[service_nr]['doc_type'])
        recipients   = xml_info[service_nr]['addresses']
        client_msgs  = xml_info[service_nr]['messages']

        doc_filename = docs_sender.set_doc_apart(type_ranges, doc_type, doc_id)

        if doc_filename:
            if not dry_run:
                if scheduling:
                    #logging.info('Scheduling: send '+os.path.basename(doc_filename)+' to client(s) of service nr '+service_nr)
                    logging.info('Agendando el envío de '+os.path.basename(doc_filename)+' a cliente(s) del servicio '+service_nr)
                    docs_sender.sched_document_mail(service_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, doc_filename)
                else:
                    #logging.info('Sending '+os.path.basename(doc_filename)+' to client(s) of service nr '+service_nr)
                    logging.info('Enviando '+os.path.basename(doc_filename)+' a cliente(s) del servicio '+service_nr)
                    docs_sender.send_document_mail(service_nr, client_name, doc_id, due_date, client_msgs, recipients, doc_type, doc_filename)
                    os.remove(doc_filename)
            else:
                if scheduling:
                    #logging.info('Without "--dryrun": Schedule send '+os.path.basename(doc_filename)+' to client(s) of service nr '+service_nr)
                    logging.info('Sin "--dryrun": Agendar el envío de '+os.path.basename(doc_filename)+' a cliente(s) del servicio '+service_nr)
                    os.remove(doc_filename)
                else:
                    #logging.info('Without "--dryrun": Send '+os.path.basename(doc_filename)+' to client(s) of service nr '+service_nr)
                    logging.info('Sin "--dryrun": Enviar '+os.path.basename(doc_filename)+' a cliente(s) del servicio '+service_nr)
                    os.remove(doc_filename)
        else:
            #logging.error('Document id {} for service {} not in the pdf files provided.'.format(doc_id, service_nr))
            logging.error('El documento {} para el servicio {} no está presente en los PDFs provistos.'.format(doc_id, service_nr))
            docs_sender.count_error(service_nr,'doc_not_in_PDFs')

    if scheduling:
        period = docs_sender.period
        period_unit = docs_sender.unit
        if period_unit == 'hours':
            period *= 60
        schedule.every(period).minutes.do(docs_sender.send_bunch)
        docs_sender.send_bunch()
        while len(schedule.jobs) > 0:
            schedule.run_pending()
            time.sleep(60)

    if not dry_run:
        docs_sender.send_report_mail()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Envía documentos de cobro (boletas o facturas) a los clientes de acuerdo al XML generado por Asyntec para la generación de documentos de cobro para usar con Abstrahere')
    parser.add_argument('--xmlfile', help='El archivo XML con la información de los servicios y las cuentas. Se considera por default un archivo con codificación ISO_8859-1. Un archivo con codificación UTF-8 rompe algunas cosas de momento.', required=True)
    parser.add_argument('--csvfile', help='El archivo CSV con la lista de los servicios cuyos documentos deben ser enviados. Las columnas adicionales se considerarán como correos adicionales a los cuales se enviará copia del correo. Se considera las columnas SERVICE, MAIL_1, MAIL_2, etc...', required=True)
    parser.add_argument('-c', '--config', help='El archivo de configuración YAML para parsear el XML y para el envío de correos.', default='config.yaml', required=False)
    parser.add_argument('-i', '--inputPDF', help='El o los archivos PDF con los documentos. Se asume que hay un documento en cada página del PDF y que cada uno es llamado T3?FXXX-YYY.pdf, con XXX e YYY los números de los documentos último y primero en el archivo. Es posible usar expansión de nombre de archivos.', required=True)
    parser.add_argument('-l', '--logfile', default='send_docs.log', help='The file to log the activity of the script', required=False)
    parser.add_argument('-v', '--verbose', default=False, help='Print detailed information about what the script is doing.', required=False)
    parser.add_argument('-s', '--schedule', help='Schedule the sending of documents on regular intervals, rather than sending them all at once.', action='store_true')
    #parser.add_argument('-s', '--schedule', default=False, help='Schedule the sending of documents on regular intervals, rather than sending them all at once.', required=False)
    parser.add_argument('-d', '--dryrun', help='Do nothing. Log detailed information about what the script would do instead.', action='store_true')
    parser.add_argument('-m', '--messages', help='Search for client tailored messages in the XML file. If present, include them in the email. Snippets to search for in messages are to be specified in the config file.', action='store_true')
    args = vars(parser.parse_args())
    main(args)

